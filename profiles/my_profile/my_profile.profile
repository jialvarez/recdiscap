
<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function my_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 */
function my_profile_install_tasks() {
  $tasks = array();

  // Add a page allowing the user to specify a "content creator" user 
  $tasks['my_profile_cc_form'] = array(
    'display_name' => st('Content creator username'),
    'type' => 'form',
  );

  return $tasks;
}

/**
 * Task callback: returns the form allowing the user to add
 * a "content creator" user
 */
function my_profile_cc_form() {
  drupal_set_title(st('Content Creator Username'));

  $form['cc_uid'] = array(
    '#type' => 'textfield',
    '#title' => st('Username for Content Creator:'),
    '#description' => st('Enter the content creator userid'),
  );
  $form['cc_email'] = array(
    '#type' => 'textfield',
    '#title' => st('Email for Content Creator:'),
    '#description' => st('Enter the content creator email'),
  );
  $form['cc_pass'] = array(
    '#type' => 'textfield',
    '#title' => st('Password for Content Creator:'),
    '#description' => st('Enter the content creator password in both fields'),
  );


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Create content creator role and user'),
    '#weight' => 15,
  );
  return $form;
}

/**
 * Submit callback: creates the "content creator" role and user
 */
function my_profile_cc_form_submit(&$form, &$form_state) {
  $uid  = $form_state['values']['cc_uid'];
  $email  = $form_state['values']['cc_email'];
  $pass  = $form_state['values']['cc_pass'];

  // Create a role for "content managers"
  $c_role = new stdClass();
  $c_role->name = 'content manager';
  $c_role->weight = 3;

  user_role_save($c_role);

  // additional permissions beyond what the authenticated
  // user receives
  user_role_grant_permissions($c_role->rid, array(
    'assign content manager role',
    'create article content',
    'edit own article content',
    'delete own article content',
    'create page content',
    'edit own page content',
    'delete own page content',
    'administer themes',
  ));
  $cc_user = array (
    'name' => $pass,
    'pass' => $pass,
    'roles' => array($c_role->rid => $c_role->rid),
    'mail' => $email,
    'status' => 1, # status: active
  );

  $user = user_save(NULL, $cc_user);
}
