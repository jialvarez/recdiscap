#!/bin/bash
yes | drush dl views
yes | drush en views

drush dl webform
yes | drush en webform

drush dl features
yes | drush en features

drush dl strongarm
yes | drush en strongarm

drush dl date
yes | drush en date
